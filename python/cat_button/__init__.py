# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Update a simple webpage with some data."""

VERSION = "0.1.0"
"""The cat-button program version string."""
