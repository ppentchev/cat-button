# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Update a "days since" webpage with some data."""

from __future__ import annotations

import dataclasses
import datetime
import os
import pathlib
import sys
import tempfile

from typing import Final

import click
import jinja2

if sys.version_info >= (3, 11):
    import tomllib
else:
    import tomli as tomllib


_FULL_DAY = 86400
"""The number of seconds in a full day."""


@dataclasses.dataclass(frozen=True, order=True)
class Event:
    """A single cat-button event."""

    tstamp: datetime.datetime
    """The moment the event occurred."""

    action: str
    """What the cat was doing at the time."""


def _read_events(events_file: pathlib.Path) -> list[Event]:
    """Read the list of events stored in a TOML file."""
    try:
        raw: Final = tomllib.load(events_file.open(mode="rb"))
    except OSError as err:
        sys.exit(f"Could not read {events_file}: {err}")
    except ValueError as err:
        sys.exit(f"Could not parse {events_file} as a valid TOML file: {err}")
    try:
        ver_min, ver_maj = raw["format"]["version"]["minor"], raw["format"]["version"]["major"]
    except (TypeError, KeyError) as err:
        sys.exit(f"Could not get format.version.major/minor from {events_file}: {err}")
    if not isinstance(ver_min, int) or not isinstance(ver_maj, int):
        sys.exit(f"Invalid format.version specification in {events_file}")
    if ver_min != 1:
        sys.exit(f"Unsupported format version {ver_min}.{ver_maj} in {events_file}")

    try:
        raw_events: Final = raw["events"]["cat-button"]
    except (TypeError, KeyError):
        sys.exit(f"No 'events' list in {events_file}")
    res: Final = []
    for raw_evt in raw_events:
        if (
            not isinstance(raw_evt, dict)
            or sorted(raw_evt.keys()) != ["action", "tstamp"]
            or not isinstance(raw_evt["action"], str)
            or not isinstance(raw_evt["tstamp"], datetime.datetime)
        ):
            sys.exit(f"Bad event structure in {events_file}: {raw_evt!r}")
        res.append(Event(tstamp=raw_evt["tstamp"], action=raw_evt["action"]))

    return sorted(res, reverse=True)


def _render_template(template_file: pathlib.Path, elapsed: int, events: list[Event]) -> str:
    """Load the Jinja2 template, fill in the events data, return the rendered contents."""
    jenv: Final = jinja2.Environment(
        autoescape=True,
        loader=jinja2.FileSystemLoader(template_file.parent),
        undefined=jinja2.StrictUndefined,
    )
    jvars: Final = {"elapsed": elapsed, "events": events}
    return jenv.get_template(template_file.name).render(**jvars) + "\n"


def _replace_file_if_needed(output_file: pathlib.Path, contents: str) -> None:
    """Check if the file already contains exactly this text, replace it otherwise."""

    def need_to_replace() -> bool:
        """Check whether we need to (re)create the file."""
        try:
            current: Final = output_file.read_text(encoding="UTF-8")
        except FileNotFoundError:
            return True
        except OSError as err:
            sys.exit(f"Could not read the {output_file} file: {err}")

        return contents != current

    if not need_to_replace():
        return

    done = False
    try:
        with tempfile.NamedTemporaryFile(
            mode="wt",
            encoding="UTF-8",
            prefix=output_file.name + ".tmp.",
            dir=output_file.parent,
            delete=True,
        ) as tempfile_obj:
            print(contents, file=tempfile_obj, end="")
            tempfile_obj.flush()
            # It's not worth creating a new `Path` object just to rename it, is it?
            os.rename(tempfile_obj.name, output_file)  # noqa: PTH104
            done = True
    except FileNotFoundError:
        if not done:
            raise


def _calc_elapsed(last: Event) -> int:
    """Get the number of full 24-hour periods since the last event's time."""
    current: Final = datetime.datetime.now(last.tstamp.tzinfo)
    return int((current.timestamp() - last.tstamp.timestamp()) / _FULL_DAY)


@click.command(name="cat-button", help="Generate a 'days since' webpage")
@click.option(
    "-e",
    "--events-file",
    type=click.Path(exists=True, dir_okay=False, resolve_path=True, path_type=pathlib.Path),
    required=True,
    help="the full path to the events.toml input file",
)
@click.option(
    "-o",
    "--output-file",
    type=click.Path(dir_okay=False, writable=True, resolve_path=True, path_type=pathlib.Path),
    required=True,
    help="the full path to the index.html output file",
)
@click.option(
    "-t",
    "--template-file",
    type=click.Path(exists=True, dir_okay=False, resolve_path=True, path_type=pathlib.Path),
    required=True,
    help="the full path to the index.html.j2 template file",
)
def main(
    *, events_file: pathlib.Path, output_file: pathlib.Path, template_file: pathlib.Path
) -> None:
    """Read the data file, recreate the index.html file."""
    events: Final = _read_events(events_file)
    if not events:
        sys.exit(f"No events declared in {events_file}")
    elapsed: Final = _calc_elapsed(events[0])
    contents: Final = _render_template(template_file, elapsed, events)
    _replace_file_if_needed(output_file, contents)
