# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

click >= 8, < 9
jinja2 >= 3, < 4
tomli >= 2, < 3; python_version < "3.11"
