# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Test the operation of the cat-button tool."""

from __future__ import annotations

import pathlib
import stat
import subprocess
import sys
import tempfile
import time

from typing import Final, TYPE_CHECKING

import click

if TYPE_CHECKING:
    import os


_OUTPUT_NAME = "test-index.html"


def _check_names(tempd: pathlib.Path, names: list[str]) -> None:
    """Make sure the directory only contains entries by those names."""
    expected: Final = sorted(names)
    try:
        actual: Final = sorted(item.name for item in tempd.iterdir())
    except OSError as err:
        sys.exit(f"Could not examine the contents of {tempd}: {err}")

    if expected != actual:
        sys.exit(f"{tempd} contents mismatch: {expected=!r} {actual=!r}")


def _run(
    events_file: pathlib.Path,
    template_file: pathlib.Path,
    output_file: pathlib.Path,
    tempd: pathlib.Path,
) -> os.stat_result:
    """Run the `cat-button` tool, get the output file's metadata."""
    output: Final = subprocess.check_output(
        ["cat-button", "-e", events_file, "-t", template_file, "-o", output_file]
    )
    if output:
        sys.exit(f"cat-button produced unexpected output: {output!r}")
    _check_names(tempd, [_OUTPUT_NAME])

    try:
        res: Final = output_file.stat()
    except OSError as err:
        sys.exit(f"Could not examine the {output_file} file after running cat-button: {err}")
    if not stat.S_ISREG(res.st_mode):
        sys.exit(
            f"cat-button created something that is not a regular file at {output_file}: "
            f"mode {res.st_mode:o}"
        )
    if not res.st_size:
        sys.exit(f"cat-button created an empty {output_file} file")

    if template_file.read_text(encoding="UTF-8") == output_file.read_text(encoding="UTF-8"):
        sys.exit("cat-button created an output file identical to the template one")

    return res


@click.command(name="test_functional", help="Test the operation of the cat-button tool")
@click.option(
    "-d",
    "--datadir",
    type=click.Path(
        exists=True, file_okay=False, dir_okay=True, path_type=pathlib.Path, resolve_path=True
    ),
    help="the path to the directory containing the template and the data file",
)
def main(*, datadir: pathlib.Path) -> None:
    """Prepare a temporary directory, run `cat-button`, see what happens."""
    events_file: Final = datadir / "events.toml"
    template_file: Final = datadir / "index.html.j2"

    with tempfile.TemporaryDirectory(prefix="test-cat-button.") as tempd_obj:
        tempd: Final = pathlib.Path(tempd_obj)
        output_file: Final = tempd / _OUTPUT_NAME
        _check_names(tempd, [])

        print(f"Running cat-button to create the {_OUTPUT_NAME} file")
        stat_first: Final = _run(events_file, template_file, output_file, tempd)

        print("Waiting for two seconds, just in case")
        time.sleep(2)

        print("Running cat-button again to make sure it does not touch the output file")
        stat_unchanged: Final = _run(events_file, template_file, output_file, tempd)
        if (
            stat_unchanged.st_mtime != stat_first.st_mtime
            or stat_unchanged.st_ctime != stat_first.st_ctime
        ):
            sys.exit(f"cat-button modified the output file: {stat_first=!r} {stat_unchanged=!r}")

    print("Looks fine!")


# We need this part, since we invoke this via `python3 -m`.
if __name__ == "__main__":
    main()
