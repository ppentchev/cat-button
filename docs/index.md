<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Generate a cat button "days since" webpage

\[[Report][report] | [Home][ringlet] | [GitLab][gitlab]\]

## Overview

Since The Cat is still in the habit of pushing The Button, record those
events in a TOML file and render a simple "days since" webpage.

This package provides the `cat-button` command-line tool that reads
a TOML file and a Jinja2 template and generates an HTML file, reporting
the number of full 24-hour periods since the last time The Cat pushed
The Button.

## Usage

The `cat-button` command-line tool requires three arguments:

- `--events-file` / `-e`: the path to the TOML file describing the events
- `--output-file` / `-o`: the path to the HTML file to generate
- `--template-file` / `-t`: the path to the Jinja2 template for
  the HTML output

The `cat-button` tool does not output anything to the standard output
stream. If the output file was generated successfully or did not need
regenerating, `cat-button` exits with code 0. If there was any kind of error,
`cat-button` reports it on the standard-error stream and exits with
a non-zero code.

The events file should have a `format.version` section containing two
integer values, `major` (1) and `minor` (0), and an `events.cat-button`
list of tables with a `tstamp` timestamp field and an `action`
human-readable comment field.

## Contact

The `cat-button` tool is developed by [Peter Pentchev][roam] in
[a GitLab repository][gitlab] and is hosted [at Ringlet][ringlet].
The current report should always be available at
[the Ringlet report site][report].

[roam]: mailto:roam@ringlet.net "Peter Pentchev"
[gitlab]: https://gitlab.com/ppentchev/cat-button "The cat-button GitLab repository"
[ringlet]: https://devel.ringlet.net/misc/cat-button/ "The Ringlet cat-button homepage"
[report]: https://things.ringlet.net/roam/cat-button/ "The current cat button report"
